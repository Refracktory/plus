package com.knunk.plus.repository;

import com.knunk.plus.repository.po.ConnectionRequestPO;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ConnectionRequestRepository extends JpaRepository<ConnectionRequestPO, Long> {
}
