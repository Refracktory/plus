package com.knunk.plus.repository.po;

import com.knunk.plus.app.types.Privacy;
import com.knunk.plus.app.types.Role;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.Email;

// insert into MEMBER (handle,
@Entity
@Table(name = "MEMBER")
public class MemberPO implements UserDetails {

  @Id
  @GeneratedValue(strategy = GenerationType.TABLE, generator = "member_generator")
  @TableGenerator(name="member_generator", table="id_generator") //, schema="plus")
  @Column(name = "MEMBER_ID")
  private Long memberId;

  @Column(name = "HANDLE")
  private String handle;

  @Column(name = "ROLE")
  private Role role;

  @Column(name = "TITLE")
  private String title;

  @Column(name = "STATUS")
  private String status;

  @Column(name = "NAME")
  private String name;

  @Column(name = "EMAIL")
  @Email private String email;

  @Column(name = "CODEWORD")
  private String codeword;

  @Column(name = "ENABLED")
  private boolean enabled = true;

  @Column(name = "INCEPTION")
  private LocalDateTime inception;

  @Column(name = "LOGIN")
  private LocalDateTime lastLogin;

  @Column(name = "LOGOUT")
  private LocalDateTime lastLogout;

  @Column(name = "LAST_IP_ADDRESS")
  private String lastIpAddress;

  @Column(name = "LOCKED")
  private boolean locked;

  @Column(name = "TOKEN")
  private String token;

  @Column(name = "PRIVACY")
  private Privacy privacy = Privacy.Private;


  public MemberPO() {
  }

  public Long getMemberId() {
    return memberId;
  }

  public void setMemberId(Long memberId) {
    this.memberId = memberId;
  }

  public void setHandle(String handle) {
    this.handle = handle;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setRole(Role role) {
    this.role = role;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getCodeword() {
    return codeword;
  }

  public void setCodeword(String codeword) {
    this.codeword = codeword;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void setEnabled(boolean enabled) {
    this.enabled = enabled;
  }

  public void setInception(LocalDateTime inception) {
    this.inception = inception;
  }

  public String getHandle() {
    return handle;
  }

  public String getName() {
    return name;
  }

  public LocalDateTime getInception() {
    return inception;
  }

  public String getEmail() {
    return email;
  }

  public Role getRole() {
    return role;
  }

  public String getStatus() {
    return status;
  }

  public LocalDateTime getLastLogin() {
    return lastLogin;
  }

  public void setLastLogin(LocalDateTime lastLogin) {
    this.lastLogin = lastLogin;
  }

  public LocalDateTime getLastLogout() {
    return lastLogout;
  }

  public void setLastLogout(LocalDateTime lastLogout) {
    this.lastLogout = lastLogout;
  }

  public String getLastIpAddress() {
    return lastIpAddress;
  }

  public void setLastIpAddress(String lastIpAddress) {
    this.lastIpAddress = lastIpAddress;
  }

  public boolean isLocked() {
    return locked;
  }

  public void setLocked(boolean locked) {
    this.locked = locked;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  /** user details implementation */
  @Override public Collection<? extends GrantedAuthority> getAuthorities() {
    Collection<GrantedAuthority> authorities = new ArrayList<>();
    authorities.add(role);
    return authorities;
  }

  @Override public String getPassword() {
    return codeword;
  }

  @Override public String getUsername() {
    return handle;
  }

  @Override public boolean isAccountNonExpired() {
    return true;
  }

  @Override public boolean isAccountNonLocked() {
    return !locked;
  }

  @Override public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override public boolean isEnabled() {
    return enabled;
  }

  public Privacy getPrivacy() {
    return privacy;
  }

  public void setPrivacy(Privacy privacy) {
    this.privacy = privacy;
  }

  public boolean isPrivate() {
    return Privacy.Private.equals(privacy);
  }

  public boolean isPublic() {
    return Privacy.Public.equals(privacy);
  }
}
