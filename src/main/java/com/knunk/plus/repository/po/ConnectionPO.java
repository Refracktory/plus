package com.knunk.plus.repository.po;

import com.knunk.plus.app.types.ConnectionType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "CONNECTION")
@IdClass(ConnectionPoCompositeKey.class)
public class ConnectionPO {

  @Id
  @Column(name = "FROM_MEMBER")
  public Long fromMemberId;

  @Id
  @Column(name = "TO_MEMBER")
  public Long toMemberId;

  @Column(name = "CONNECTION_TYPE")
  public ConnectionType connectionType;

  public Long getFromMemberId() {
    return fromMemberId;
  }

  public void setFromMemberId(Long fromMemberId) {
    this.fromMemberId = fromMemberId;
  }

  public Long getToMemberId() {
    return toMemberId;
  }

  public void setToMemberId(Long toMemberId) {
    this.toMemberId = toMemberId;
  }

  public ConnectionType getConnectionType() {
    return connectionType;
  }

  public void setConnectionType(ConnectionType connectionType) {
    this.connectionType = connectionType;
  }

}
