package com.knunk.plus.repository.po;

import java.io.Serializable;

public class ConnectionPoCompositeKey implements Serializable {

  private static final long serialVersionUID = -2838927368196818372L;

  public Long fromMemberId;
  public Long toMemberId;

  public ConnectionPoCompositeKey() {
    super();
  }

  public ConnectionPoCompositeKey(Long fromMember, Long toMember) {
    this.fromMemberId = fromMember;
    this.toMemberId = toMember;
  }

  public Long getFromMemberId() {
    return fromMemberId;
  }

  public void setFromMemberId(Long fromMemberId) {
    this.fromMemberId = fromMemberId;
  }

  public Long getToMemberId() {
    return toMemberId;
  }

  public void setToMemberId(Long toMemberId) {
    this.toMemberId = toMemberId;
  }

}
