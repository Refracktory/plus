package com.knunk.plus.repository.po;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name = "CONNECTION_REQUEST")
public class ConnectionRequestPO {

  @Id
  @GeneratedValue(strategy = GenerationType.TABLE, generator = "connection_request_generator")
  @TableGenerator(name="connection_request_generator", table="id_generator") //, schema="plus")
  @Column(name = "CONNECTION_REQUEST_ID")
  private Long connectionRequestId;

  @Column(name = "REQUESTED_BY")
  private String requestedBy;

  @Column(name = "REQUESTED_OF")
  private String requestedOf;

  @Column(name = "REQUEST_TIME")
  private LocalDateTime requestTime;

  @Column(name = "REQUEST_ACCEPTED_TIME")
  private LocalDateTime requestAcceptedTime;

  public String getRequestedBy() {
    return requestedBy;
  }

  public void setRequestedBy(String requestedBy) {
    this.requestedBy = requestedBy;
  }

  public String getRequestedOf() {
    return requestedOf;
  }

  public void setRequestedOf(String requestedOf) {
    this.requestedOf = requestedOf;
  }

  public LocalDateTime getRequestTime() {
    return requestTime;
  }

  public void setRequestTime(LocalDateTime requestTime) {
    this.requestTime = requestTime;
  }

  public LocalDateTime getRequestAcceptedTime() {
    return requestAcceptedTime;
  }

  public void setRequestAcceptedTime(LocalDateTime requestAcceptedTime) {
    this.requestAcceptedTime = requestAcceptedTime;
  }

}
