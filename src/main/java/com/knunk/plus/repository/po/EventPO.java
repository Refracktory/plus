package com.knunk.plus.repository.po;

import com.knunk.plus.app.types.EventType;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.servlet.http.HttpServletRequest;

@Entity
@Table(name = "EVENT")
public class EventPO {

  @Id
  @GeneratedValue(strategy = GenerationType.TABLE, generator = "event_generator")
  @TableGenerator(name="event_generator", table="id_generator") //, schema="plus")
  @Column(name = "EVENT_ID")
  private Long eventId;

  @Column(name = "EVENT_OWNER")
  private String eventOwner;

  @Column(name = "EVENT_TYPE")
  private EventType eventType;

  @Column(name = "EVENT_TIME")
  private LocalDateTime eventTime;

  @Column(name ="EVENT_TEXT")
  private String eventText;

  @Column(name = "EVENT_NOTE")
  private String eventNote;

  @Column(name = "EVENT_ORIGIN")
  private String eventOrigin;

  @Column(name = "EVENT_ACKNOWLEDGE")
  private Boolean eventAcknowledge;

  public EventPO() {
    super();
  }

  public EventPO apply(HttpServletRequest request) {
    eventOrigin = request.getRemoteAddr();
    return this;
  }

  public static Map<String, String> getNewParameterMap() {
    Map<String, String> map = new HashMap<>();
    return map;
  }

  public static EventPO create(EventType eventType, Map<String, String> varsMap) {
    EventPO eventPO = new EventPO(eventType);

    String msg = eventType.getTemplate();
    for (String key : varsMap.keySet()) {
      msg = msg.replace("{" + key + "}", varsMap.get(key));
    }
    if (msg.contains("{") || msg.contains("}")) {
      String eventNote = "failed to properly substitute EventType template " + eventType.getTemplate() + " with " + varsMap;
      System.out.println(eventNote);
      eventPO.setEventNote(eventNote);
    }
    eventPO.setEventText(msg);
    return eventPO;
  }

  public EventPO(EventType eventType) {
    this.eventType = eventType;
    this.eventTime = LocalDateTime.now();
  }

  public String getEventNote() {
    return eventNote;
  }

  public void setEventNote(String eventNote) {
    this.eventNote = eventNote;
  }

  public String getEventOrigin() {
    return eventOrigin;
  }

  public void setEventOrigin(String eventOrigin) {
    this.eventOrigin = eventOrigin;
  }

  public Boolean getEventAcknowledge() {
    return eventAcknowledge;
  }

  public void setEventAcknowledge(Boolean eventAcknowledge) {
    this.eventAcknowledge = eventAcknowledge;
  }

  public Long getEventId() {
    return eventId;
  }

  public EventType getEventType() {
    return eventType;
  }

  public void setEventType(EventType eventType) {
    this.eventType = eventType;
  }

  public String getEventOwner() {
    return eventOwner;
  }

  public void setEventOwner(String eventOwner) {
    this.eventOwner = eventOwner;
  }

  public LocalDateTime getEventTime() {
    return eventTime;
  }

  public void setEventTime(LocalDateTime eventTime) {
    this.eventTime = eventTime;
  }

  public String getEventText() {
    return eventText;
  }

  public void setEventText(String eventText) {
    this.eventText = eventText;
  }

}
