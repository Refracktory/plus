package com.knunk.plus.repository;

import com.knunk.plus.app.types.Role;
import com.knunk.plus.repository.po.MemberPO;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MemberRepository extends JpaRepository<MemberPO, Long> {
  public MemberPO findMemberPOByName(String name);
  public MemberPO findMemberPOByHandle(String handle);
  public List<MemberPO> findAllByRole(Role role);
  public List<MemberPO> findAllByEmail(String email);
}
