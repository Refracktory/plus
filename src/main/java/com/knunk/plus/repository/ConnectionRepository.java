package com.knunk.plus.repository;

import com.knunk.plus.repository.po.ConnectionPO;
import com.knunk.plus.repository.po.ConnectionPoCompositeKey;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ConnectionRepository extends JpaRepository<ConnectionPO, ConnectionPoCompositeKey> {

  @Query("select p.toMemberId from CollectionPO p where p.fromMemberId = :fromMemberId")
  List<Long> getAllConnectionIds(@Param("fromMemberId") long fromMemberId);

  List<ConnectionPO> findAllByFromMemberId(long fromMemberId);
  List<ConnectionPO> findAllByFromMemberIds(List<Long> fromMemberId);

  @Query("select p.toMemberId from CollectionPO p where p.fromMemberId = :fromMemberId")
  List<ConnectionPO> findOverlap(Long memberId, Long memberId1);
}
