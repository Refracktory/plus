package com.knunk.plus.repository;

import com.knunk.plus.app.types.EventType;
import com.knunk.plus.repository.po.EventPO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;

public interface EventRepository extends JpaRepository<EventPO, Long> {
  Page<EventPO> findAllByEventType(EventType eventType, Pageable page);
  Page<EventPO> findAllByEventOwner(String owner, Pageable page);
  Page<EventPO> findAllByEventTimeAfter(LocalDateTime startTime, Pageable page);
  Page<EventPO> findAllByEventTypeAndEventTimeAfter(EventType eventType, LocalDateTime startTime, Pageable page);
  Page<EventPO> findAllByEventTypeAndEventTimeAfterAndEventTimeBefore(EventType eventType, LocalDateTime startTime, LocalDateTime endTime, Pageable page);
}
