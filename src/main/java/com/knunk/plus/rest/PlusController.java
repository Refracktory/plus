package com.knunk.plus.rest;

import com.knunk.plus.rest.objects.MessageRequest;
import com.knunk.plus.rest.objects.PageResponse;
import com.knunk.plus.services.PlusMessageService;
import com.knunk.plus.services.PlusService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/plus")
public class PlusController {

  @Autowired
  private PlusService plusService;

  @Autowired
  private PlusMessageService plusMessageService;

  @RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public RedirectView handleIndex() {
    return new RedirectView("/plus/welcome");
  }

  @RequestMapping(value = "/goodbye", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public ResponseEntity<PageResponse> goodbye(HttpServletRequest request) {
    return ResponseEntity.ok(new PageResponse("Goodbye, Plus!"));
  }

  @RequestMapping(value = "/hello", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public ResponseEntity<PageResponse> hello(HttpServletRequest request) {
    return ResponseEntity.ok(new PageResponse("Hello, Plus!"));
  }

  /** REAL METHODS */
  @RequestMapping(value = "/send", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public ResponseEntity<PageResponse> sendMessageToMember(@RequestBody @Valid MessageRequest messageRequest, HttpServletRequest request) throws Exception {
    return plusMessageService.sendMessage(messageRequest, request.getRemoteUser());
  }

}
