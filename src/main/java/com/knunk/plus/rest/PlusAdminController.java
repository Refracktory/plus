package com.knunk.plus.rest;

import com.knunk.plus.app.types.Role;
import com.knunk.plus.repository.EventRepository;
import com.knunk.plus.repository.MemberRepository;
import com.knunk.plus.repository.po.EventPO;
import com.knunk.plus.repository.po.MemberPO;
import com.knunk.plus.rest.objects.LoginRequest;
import com.knunk.plus.rest.objects.MessageRequest;
import com.knunk.plus.rest.objects.PageListResponse;
import com.knunk.plus.rest.objects.PageResponse;
import com.knunk.plus.rest.objects.ViewEventRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/admin")
public class PlusAdminController {

  private static Logger LOGGER = LoggerFactory.getLogger(PlusAdminController.class);

  @Autowired
  private MemberRepository memberRepository;

  @Autowired
  private EventRepository eventRepository;

  @RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public ResponseEntity<PageResponse> deleteMember(@RequestBody @Valid LoginRequest loginRequest, HttpServletRequest request) throws Exception {
    MemberPO memberPO = memberRepository.findMemberPOByHandle(loginRequest.getHandle());

    if (memberPO == null) {
      return ResponseEntity.status(409).header("message", "User does not exist").body(new PageResponse("hey, you already tried that!?"));
    }

    if (Role.Owner.equals(memberPO.getRole())) {
      return ResponseEntity.status(409).header("message", "Cannot delete owner account").body(new PageResponse("deleteing owner account is prohibited"));
    }
    memberRepository.delete(memberPO);
    return ResponseEntity.ok(new PageResponse("so sorry to see you go, " + memberPO.getName()));
  }

  @RequestMapping(value = "/lock", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public ResponseEntity<PageResponse> lockMember(@RequestBody @Valid LoginRequest loginRequest, HttpServletRequest request) throws Exception {
    MemberPO memberPO = memberRepository.findMemberPOByHandle(loginRequest.getHandle());
    if (memberPO == null) {
      return ResponseEntity.status(409).header("message", "User does not exist").body(new PageResponse("hey, you already tried that!?"));
    }

    if (memberPO.getRole().equals(Role.Owner)) {
      return ResponseEntity.status(409).header("message", "Cannot lock owner account").body(new PageResponse("locking owner account is prohibited"));
    }
    if (memberPO.isLocked()) {
      return ResponseEntity.status(409).header("message", "User is already locked").body(new PageResponse("this account is currently locked"));
    }
    if (!memberPO.isEnabled()) {
      return ResponseEntity.status(409).header("message", "User is already disabled").body(new PageResponse("this account is currently disabled"));
    }

    memberPO.setLocked(true);
    memberPO.setEnabled(false);
    memberRepository.save(memberPO);
    memberPO.setCodeword("locked");
    return ResponseEntity.ok(new PageResponse("locked out " + memberPO.getName()));
  }

  @RequestMapping(value = "/unlock", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public ResponseEntity<PageResponse> unlockMember(@RequestBody @Valid LoginRequest loginRequest, HttpServletRequest request) throws Exception {
    MemberPO memberPO = memberRepository.findMemberPOByHandle(loginRequest.getHandle());
    if (memberPO == null) {
      return ResponseEntity.status(409).header("message", "User does not exist").body(new PageResponse("hey, you already tried that!?"));
    }
    if (!memberPO.isLocked()) {
      return ResponseEntity.status(409).header("message", "User is not locked").body(new PageResponse("this account is not currently locked"));
    }

    memberPO.setLocked(false);
    memberPO.setEnabled(true);
    memberRepository.save(memberPO);
    memberPO.setCodeword("unlocked");
    return ResponseEntity.ok(new PageResponse("locked out " + memberPO.getName()));
  }
  @RequestMapping(value = "/send", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public ResponseEntity<PageResponse> sendMessageToMember(@RequestBody @Valid MessageRequest messageRequest, HttpServletRequest request) throws Exception {
    MemberPO memberPO = memberRepository.findMemberPOByHandle(messageRequest.getHandle());
    if (memberPO == null) {
      return ResponseEntity.status(404).header("message", "User does not exist").build();
    }
    return ResponseEntity.status(200).body(new PageResponse(String.format("your text only message to %s has been sent", messageRequest.getHandle())));
  }

  @RequestMapping(value = "/view/user", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public ResponseEntity<MemberPO> viewMember(@RequestBody @Valid LoginRequest loginRequest, HttpServletRequest request) throws Exception {
    MemberPO memberPO = memberRepository.findMemberPOByHandle(loginRequest.getHandle());
    if (memberPO == null) {
      return ResponseEntity.status(404).header("message", "User does not exist").build();
    }
    memberPO.setCodeword("******");
    return ResponseEntity.status(200).body(memberPO);
  }

  @RequestMapping(value = "/view/users", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public ResponseEntity<List<MemberPO>> viewMembers(@RequestBody @Valid LoginRequest loginRequest, HttpServletRequest request) throws Exception {
    List<MemberPO> members = memberRepository.findAll();
    for (MemberPO memberPO :members) {
      memberPO.setCodeword("******");
    }
    return ResponseEntity.status(200).body(members);
  }

  @RequestMapping(value = "/view/events", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public ResponseEntity<PageListResponse<EventPO>> viewEvents(
    @RequestBody @Valid ViewEventRequest viewEventRequest, HttpServletRequest request) throws Exception {

    Pageable page = PageRequest.of(viewEventRequest.getCurrentPage(), viewEventRequest.getPageSize());
    Page<EventPO> events = null;
    if (viewEventRequest.getEventType() != null && viewEventRequest.getStartTime() != null) {
      events = eventRepository.findAllByEventTypeAndEventTimeAfter(viewEventRequest.getEventType(), viewEventRequest.getStartTime(), page);
    } else if (viewEventRequest.getStartTime() != null) {
      events = eventRepository.findAllByEventTimeAfter(viewEventRequest.getStartTime(), page);
    } else if (viewEventRequest.getEventType() != null) {
      events = eventRepository.findAllByEventType(viewEventRequest.getEventType(), page);
    }

    if (events != null && events.getTotalPages() == 0) {
      LOGGER.info("found no events");
      return ResponseEntity.status(204).header("message", "no events match criteria").build();
    } else if (events == null) {
      LOGGER.info("found no events");
      return ResponseEntity.status(204).header("message", "search criteria did not resolve to any events").build();
    }

    LOGGER.info(String.format("found %d events", events.getTotalElements()));
    PageListResponse<EventPO> body = new PageListResponse<EventPO>(events.getContent());
    body.setCurrentPage(events.getPageable().getPageNumber());
    body.setPageSize(events.getPageable().getPageSize());
    return ResponseEntity.status(200).body(body);
  }

}
