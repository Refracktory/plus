package com.knunk.plus.rest;

import com.knunk.plus.rest.objects.LoginRequest;
import com.knunk.plus.rest.objects.MessageRequest;
import com.knunk.plus.rest.objects.PageResponse;
import com.knunk.plus.services.PlusLoginService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/log")
public class PlusLoginController {

  @Autowired
  private PlusLoginService plusLoginService;

  @RequestMapping(value = "/in", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public ResponseEntity<PageResponse> login(@RequestBody LoginRequest loginRequest, HttpServletRequest request) throws Exception {
    return plusLoginService.doLogin(loginRequest);
  }

  @RequestMapping(value = "/out", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public ResponseEntity<PageResponse> logout(@RequestBody LoginRequest loginRequest, HttpServletRequest request) throws Exception {
    return plusLoginService.doLogout(loginRequest);
  }

  @RequestMapping(value = "/reset", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public ResponseEntity<PageResponse> reset(@RequestBody @Valid MessageRequest resetRequest, HttpServletRequest request) throws Exception {
    return plusLoginService.doResetLogin(resetRequest, request);
  }

}
