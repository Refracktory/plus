package com.knunk.plus.rest.objects;

import com.knunk.plus.config.Constants;

import javax.validation.constraints.Pattern;

public class LoginRequest {

  @Pattern(regexp = Constants.ILLEGAL_CHARACTER_REGEX, message = "handle" + Constants.ILLEGAL_CHARACTER_REGEX_NAMES_TEXT)
  private String handle;

  @Pattern(regexp = Constants.ILLEGAL_CHARACTER_REGEX, message = "code" + Constants.ILLEGAL_CHARACTER_REGEX_NAMES_TEXT)
  private String code;

  public void setHandle(String handle) {
    this.handle = handle;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getHandle() {
    return handle;
  }

  public String getCode() {
    return code;
  }
}
