package com.knunk.plus.rest.objects;

import java.util.List;

public class PageListResponse<T> {
  private List<T> list;

  private int currentPage = 1;
  private int pageSize = 20;

  public PageListResponse(List<T> list) {
    this.list = list;
  }

  public List<T> getList() {
    return list;
  }

  public int getCurrentPage() {
    return currentPage;
  }

  public void setCurrentPage(int currentPage) {
    this.currentPage = currentPage;
  }

  public int getPageSize() {
    return pageSize;
  }

  public void setPageSize(int pageSize) {
    this.pageSize = pageSize;
  }


}
