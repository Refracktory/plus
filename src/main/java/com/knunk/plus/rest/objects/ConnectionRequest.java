package com.knunk.plus.rest.objects;

import java.time.LocalDateTime;

public class ConnectionRequest {

  private String fromHandle;
  private String toHandle;
  private String note;
  private LocalDateTime created;

  public String getFromHandle() {
    return fromHandle;
  }

  public void setFromHandle(String fromHandle) {
    this.fromHandle = fromHandle;
  }

  public String getToHandle() {
    return toHandle;
  }

  public void setToHandle(String toHandle) {
    this.toHandle = toHandle;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public LocalDateTime getCreated() {
    return created;
  }

  public void setCreated(LocalDateTime created) {
    this.created = created;
  }

}
