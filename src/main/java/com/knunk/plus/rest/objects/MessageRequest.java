package com.knunk.plus.rest.objects;

import com.knunk.plus.config.Constants;

import javax.validation.constraints.Pattern;

public class MessageRequest {
  @Pattern(regexp = Constants.ILLEGAL_CHARACTER_REGEX, message = "handle" + Constants.ILLEGAL_CHARACTER_REGEX_NAMES_TEXT)
  private String handle;

  public String getHandle() {
    return handle;
  }

  public void setHandle(String handle) {
    this.handle = handle;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  private String message;

}
