package com.knunk.plus.rest.objects;

import com.knunk.plus.config.Constants;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class RegistrationRequest extends LoginRequest {

  @NotEmpty
  @Pattern(regexp = Constants.ILLEGAL_CHARACTER_REGEX, message = "name" + Constants.ILLEGAL_CHARACTER_REGEX_NAMES_TEXT)
  private String name;

  @Email(regexp = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])")
  private String email;

  @Pattern(regexp = Constants.ILLEGAL_CHARACTER_REGEX, message = "code" + Constants.ILLEGAL_CHARACTER_REGEX_NAMES_TEXT)
  @NotEmpty
  private String codeword;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getCodeword() {
    return codeword;
  }

  public void setCodeword(String codeword) {
    this.codeword = codeword;
  }

}
