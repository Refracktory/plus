package com.knunk.plus.rest.objects;

import com.knunk.plus.app.types.EventType;

import java.time.LocalDateTime;

public class ViewEventRequest {

  private EventType eventType;
  private LocalDateTime startTime;
  private LocalDateTime endTime;
  private String owner;
  private String messageSearchText;
  private int currentPage = 0;
  private int pageSize = 50;

  public EventType getEventType() {
    return eventType;
  }

  public void setEventType(EventType eventType) {
    this.eventType = eventType;
  }

  public LocalDateTime getStartTime() {
    return startTime;
  }

  public void setStartTime(LocalDateTime startTime) {
    this.startTime = startTime;
  }

  public LocalDateTime getEndTime() {
    return endTime;
  }

  public void setEndTime(LocalDateTime endTime) {
    this.endTime = endTime;
  }

  public String getOwner() {
    return owner;
  }

  public void setOwner(String owner) {
    this.owner = owner;
  }

  public String getMessageSearchText() {
    return messageSearchText;
  }

  public void setMessageSearchText(String messageSearchText) {
    this.messageSearchText = messageSearchText;
  }

  public int getCurrentPage() {
    return currentPage;
  }

  public void setCurrentPage(int currentPage) {
    this.currentPage = currentPage;
  }

  public int getPageSize() {
    return pageSize;
  }

  public void setPageSize(int pageSize) {
    this.pageSize = pageSize;
  }
}
