package com.knunk.plus.rest.objects;

public class PageResponse {
  private String text = "Welcome to Plus";

  public PageResponse(String text) {
    this.text = text;
  }
  public String getText() {
    return text;
  }

}
