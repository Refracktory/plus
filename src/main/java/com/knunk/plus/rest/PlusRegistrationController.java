package com.knunk.plus.rest;

import com.knunk.plus.rest.objects.PageResponse;
import com.knunk.plus.rest.objects.RegistrationRequest;
import com.knunk.plus.services.PlusRegistrationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/register")
public class PlusRegistrationController {

  @Autowired
  private PlusRegistrationService plusRegistrationService;

  @RequestMapping(value = "/new", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public ResponseEntity<PageResponse> newMember(@RequestBody @Valid RegistrationRequest registrationRequest, HttpServletRequest request) throws Exception {

    return plusRegistrationService.doRegistration(registrationRequest);
  }
}
