package com.knunk.plus.rest;


import com.knunk.plus.app.types.ConnectionType;
import com.knunk.plus.app.types.Privacy;
import com.knunk.plus.repository.MemberRepository;
import com.knunk.plus.repository.po.ConnectionPO;
import com.knunk.plus.repository.po.MemberPO;
import com.knunk.plus.rest.objects.ConnectionRequest;
import com.knunk.plus.rest.objects.PageResponse;
import com.knunk.plus.services.PlusConnectionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/connection")
public class PlusConnectionController {

  @Autowired
  private PlusConnectionService plusConnectionService;

  @Autowired
  private MemberRepository memberRepository;

  @RequestMapping(value = "/request", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public ResponseEntity<PageResponse> requestConnection(ConnectionRequest connectionRequest, HttpServletRequest request) {
    String from = connectionRequest.getFromHandle();
    String to = connectionRequest.getToHandle();
    MemberPO toMember = memberRepository.findMemberPOByHandle(to);

    ConnectionType connectionType = plusConnectionService.getConnection(from, to) ;
    if (connectionType.areConnected()) {
      return ResponseEntity.status(202).body(new PageResponse("you are already connected to " + to));
    }

    if (toMember.isPrivate()) {
      return ResponseEntity.status(412).body(new PageResponse("user does not accept connection requests"));
    } else if (Privacy.Friends.equals(toMember.getPrivacy())) {
      return ResponseEntity.status(412).body(new PageResponse("user does not accept unfamiliar connection requests"));
    } else if (Privacy.FriendOfFriends.equals(toMember.getPrivacy())) {
      if (plusConnectionService.friendsOverlap(from, to)) {

      } else {
        return ResponseEntity.status(202).body(new PageResponse("could not adequately determine connectivity to " + to));
      }
    }
    return ResponseEntity.ok(new PageResponse("your request has been sent"));
  }

}
