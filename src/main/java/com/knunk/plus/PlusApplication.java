package com.knunk.plus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@Profile(value = {"deployed", "local"})
public class PlusApplication {

  public static void main(String[] args) {
    SpringApplication.run(PlusApplication.class, args);
  }

}
