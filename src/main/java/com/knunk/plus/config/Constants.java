package com.knunk.plus.config;

public class Constants {
  public static final String ILLEGAL_CHARACTER_REGEX = "^[^:'\"]*$";
  public static final String ILLEGAL_CHARACTER_REGEX_NAMES_TEXT = " may not contain colon, single or double quote";
}
