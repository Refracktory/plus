package com.knunk.plus.config;

import com.knunk.plus.repository.MemberRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class PlusAuthenticationConfiguration extends GlobalAuthenticationConfigurerAdapter {
  private @Autowired MemberRepository memberRepository;
  private Logger logger = LoggerFactory.getLogger(this.getClass());

  // this is the important part:
  @Bean(name = "plusAuthenticationProvider")
  public DaoAuthenticationProvider authProvider() {
    final DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
    authProvider.setUserDetailsService(getUserDetailsService());
    authProvider.setPasswordEncoder(getPasswordEncoder());
    return authProvider;
  }

  /** THIS WORKS!! */
  @Bean
  public UserDetailsService getUserDetailsService() {
    UserDetailsService userDetailsService = new UserDetailsService() {
      @Override public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        logger.debug("checking for " + name);
        UserDetails ud = memberRepository.findMemberPOByHandle(name);
        if (ud == null) {
          throw new UsernameNotFoundException("user not found");
        }
        logger.debug("has authorities: " + ud.getAuthorities());
        return ud;
      }
    };
    return userDetailsService;
  }

  @Bean
  @Profile("insecure")
  public PasswordEncoder getPasswordEncoder() {
    // FIXME
    logger.warn("using no op password encoder");
    return NoOpPasswordEncoder.getInstance();
    //return new BCryptPasswordEncoder();
  }

  @Bean
  @Profile("default")
  public PasswordEncoder getBCryptPasswordEncoder() {
    logger.info("using BCrypt password encoder");
    return new BCryptPasswordEncoder();
  }

  @Override
  public void init(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(getUserDetailsService())
      .passwordEncoder(getPasswordEncoder());
  }
}