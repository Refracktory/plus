package com.knunk.plus.services;

import com.knunk.plus.app.types.ConnectionType;
import com.knunk.plus.repository.ConnectionRepository;
import com.knunk.plus.repository.MemberRepository;
import com.knunk.plus.repository.po.ConnectionPO;
import com.knunk.plus.repository.po.ConnectionPoCompositeKey;
import com.knunk.plus.repository.po.MemberPO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PlusConnectionService {

  @Autowired
  private ConnectionRepository connectionRepository;

  @Autowired
  private MemberRepository memberRepository;

  public ConnectionType getConnection(String fromHandle, String toHandle) {
    MemberPO from = memberRepository.findMemberPOByHandle(fromHandle);
    MemberPO to = memberRepository.findMemberPOByHandle(toHandle);
    if (from != null && to != null) {
      ConnectionPoCompositeKey key = new ConnectionPoCompositeKey();
      Optional<ConnectionPO> linkagePO = connectionRepository.findById(key);
      if (linkagePO.isPresent()) {
        return linkagePO.get().connectionType;
      }
      return ConnectionType.None;
    }
    return ConnectionType.None;

  }

  public List<ConnectionPO> getFirstDegreeConnections(String handle) {
    MemberPO member = memberRepository.findMemberPOByHandle(handle);
    List<ConnectionPO> connections = connectionRepository.findAllByFromMemberId(member.getMemberId());
    return connections;
  }

  /**
   * this doesn't work.
   */
  public List<ConnectionPO> getSecondDegreeConnections(String handle) {
    MemberPO member = memberRepository.findMemberPOByHandle(handle);
    List<Long> connectionIds = connectionRepository.getAllConnectionIds(member.getMemberId());
    return connectionRepository.findAllByFromMemberIds(connectionIds);
  }

  public boolean friendsOverlap(String from, String to) {
    MemberPO fromMember = memberRepository.findMemberPOByHandle(from);
    MemberPO toMember = memberRepository.findMemberPOByHandle(to);
    List<ConnectionPO> list = connectionRepository.findOverlap(fromMember.getMemberId(), toMember.getMemberId());
    return list.size() > 0;
  }
}
