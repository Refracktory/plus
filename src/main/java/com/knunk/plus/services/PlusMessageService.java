package com.knunk.plus.services;

import com.knunk.plus.repository.MemberRepository;
import com.knunk.plus.repository.po.MemberPO;
import com.knunk.plus.rest.objects.MessageRequest;
import com.knunk.plus.rest.objects.PageResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

@Service
public class PlusMessageService {

  private static Logger LOGGER = LoggerFactory.getLogger(PlusMessageService.class);

  @Autowired
  private PlusConnectionService plusConnectionService;

  @Autowired
  private MemberRepository memberRepository;

  public ResponseEntity<PageResponse> sendMessage(@RequestBody @Valid MessageRequest messageRequest, String fromHandle) {
    MemberPO memberPO = memberRepository.findMemberPOByHandle(messageRequest.getHandle());
    if (memberPO == null) {

      return ResponseEntity.status(404).header("message", "User does not exist").build();

    } else if (memberPO.isPrivate()) {

      return ResponseEntity.status(202).body(new PageResponse("you may not send messages to the specified user"));

    } else if (memberPO.isPublic() && plusConnectionService.getConnection(memberPO.getHandle(), fromHandle).areConnected()) {

      LOGGER.info("'sending' message to " + messageRequest.getHandle());
      return ResponseEntity.status(200).body(new PageResponse(String.format("your text only message to %s has been sent", messageRequest.getHandle())));

    }
    return ResponseEntity.status(202).body(new PageResponse("you must first connect with this user to communicate"));

  }

}
