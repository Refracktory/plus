package com.knunk.plus.services;

import com.knunk.plus.app.types.Privacy;
import com.knunk.plus.app.types.Role;
import com.knunk.plus.repository.MemberRepository;
import com.knunk.plus.repository.po.MemberPO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

@Service
public class PlusService implements ApplicationListener<ApplicationStartedEvent> {

  private static Logger LOGGER = LoggerFactory.getLogger(PlusService.class);

  @Autowired
  private MemberRepository memberRepository;

  @Override public void onApplicationEvent(ApplicationStartedEvent applicationStartedEvent) {

    // test for Owner and Admin accounts. if there aren't any, add the defaults.
    List<MemberPO> admins = memberRepository.findAllByRole(Role.Admin);
    LOGGER.info(String.format("there are currently %d admins", admins.size()));
    if (admins.size() == 0) {
      MemberPO defaultAdmin = new MemberPO();
      defaultAdmin.setCodeword("admin");
      defaultAdmin.setHandle("admin");
      defaultAdmin.setEmail("refracktory@gmail.com");
      defaultAdmin.setName("refracktory.org");
      defaultAdmin.setRole(Role.Admin);
      defaultAdmin.setPrivacy(Privacy.Public);
      memberRepository.save(defaultAdmin);
      LOGGER.info("no admin account found. added default admin account admin / admin");
    }
    List<MemberPO> owners = memberRepository.findAllByRole(Role.Owner);
    LOGGER.info(String.format("there are currently %d owners", owners.size()));
    if (owners.size() == 0) {
      MemberPO defaultOwner = new MemberPO();
      defaultOwner.setCodeword("owner");
      defaultOwner.setHandle("owner");
      defaultOwner.setEmail("refracktory@gmail.com");
      defaultOwner.setName("refracktory.org");
      defaultOwner.setRole(Role.Owner);
      defaultOwner.setPrivacy(Privacy.Public);
      memberRepository.save(defaultOwner);
      LOGGER.info("no owner account found. added default owner account owner / owner");
    }
    List<MemberPO> users = memberRepository.findAllByRole(Role.User);
    if (users.size() == 0) {
      MemberPO defaultUser = new MemberPO();
      defaultUser.setHandle("refracktory");
      defaultUser.setCodeword("refracktory");
      defaultUser.setEmail("refracktory@gmail.com");
      defaultUser.setName("Rey");
      defaultUser.setRole(Role.User);
      defaultUser.setPrivacy(Privacy.Public);
      memberRepository.save(defaultUser);
      LOGGER.info("added default user account refracktory / refracktory");
    }

    addTestUser("groupie1", Privacy.Public);
    addTestUser("groupie2", Privacy.Public);
    addTestUser("groupieP", Privacy.Private);
    addTestUser("groupieF", Privacy.Friends);
    addTestUser("groupieFoF", Privacy.FriendOfFriends);

    LOGGER.info("Plus has started.");
  }

  private void addTestUser(String handle, Privacy privacy) {
    LOGGER.info(String.format("adding test user: '%s' with privacy: %s", handle, privacy));
    MemberPO defaultUser = new MemberPO();
    defaultUser.setHandle(handle);
    defaultUser.setCodeword(handle);
    defaultUser.setEmail(handle + "@refracktory.org");
    defaultUser.setName(handle);
    defaultUser.setRole(Role.User);
    defaultUser.setPrivacy(privacy);
    memberRepository.save(defaultUser);

  }
  // for password resets?
  private String getARandomFirstWord() {
    String wordlist = "red,blue,green,yellow,black,white,purple,orange,pink,violet,ultra,minor,major,super,ultra";
    String[] words = wordlist.split(",");
    return words[new Random().nextInt(words.length - 1)];
  }

  private String getARandomSecondWord() {
    String wordlist = "2112,lenses,barchetta,star,secret,geddy,alex,neil,rush,lerxt";
    String[] words = wordlist.split(",");
    return words[new Random().nextInt(words.length - 1)];
  }

}
