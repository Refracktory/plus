package com.knunk.plus.services;

import com.knunk.plus.app.types.EventType;
import com.knunk.plus.repository.EventRepository;
import com.knunk.plus.repository.MemberRepository;
import com.knunk.plus.repository.po.EventPO;
import com.knunk.plus.repository.po.MemberPO;
import com.knunk.plus.rest.objects.LoginRequest;
import com.knunk.plus.rest.objects.MessageRequest;
import com.knunk.plus.rest.objects.PageResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Service
public class PlusLoginService {

  @Autowired
  private MemberRepository memberRepository;

  @Autowired
  private EventRepository eventRepository;

  public ResponseEntity<PageResponse> doLogin(@RequestBody LoginRequest loginRequest) {
    MemberPO memberPO = memberRepository.findMemberPOByHandle(loginRequest.getHandle());
    if (memberPO != null) {
      if (memberPO.isEnabled() && !memberPO.isLocked()) {
        memberPO.setLastLogin(LocalDateTime.now());
        memberPO.setToken(UUID.randomUUID().toString());
        memberRepository.save(memberPO);

        Map<String, String> vars = EventPO.getNewParameterMap();
        vars.put("handle", loginRequest.getHandle());
        EventPO eventPO = EventPO.create(EventType.USER_LOGIN, vars);
        eventPO.setEventOwner(loginRequest.getHandle());
        eventRepository.save(eventPO);

        PageResponse p = new PageResponse("you logged in, " + memberPO.getName() + "!");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Plus-Token", memberPO.getToken());
        ResponseEntity<PageResponse> pr = new ResponseEntity(p, headers, HttpStatus.OK);
        return pr;
      } else {
        return ResponseEntity.badRequest().body(new PageResponse("this account is not available. please contact the admin."));
      }
    }
    return ResponseEntity.badRequest().body(new PageResponse("you need to join first"));
  }

  public ResponseEntity<PageResponse> doLogout(@RequestBody LoginRequest loginRequest) {
    MemberPO memberPO = memberRepository.findMemberPOByHandle(loginRequest.getHandle());
    if (memberPO != null) {
      memberPO.setLastLogout(LocalDateTime.now());
      memberRepository.save(memberPO);

      Map<String, String> vars = EventPO.getNewParameterMap();
      vars.put("handle", loginRequest.getHandle());
      EventPO eventPO = EventPO.create(EventType.USER_LOGIN, vars);
      eventPO.setEventOwner(loginRequest.getHandle());
      eventRepository.save(eventPO);

      return ResponseEntity.ok(new PageResponse( "you logged out, " + memberPO.getName() + "!"));
    }
    return ResponseEntity.ok(new PageResponse("logout accepted"));
  }

  public ResponseEntity<PageResponse> doResetLogin(@RequestBody @Valid MessageRequest resetRequest, HttpServletRequest request) {
    MemberPO memberPO = memberRepository.findMemberPOByHandle(resetRequest.getHandle());
    if (memberPO != null) {

      Map<String, String> vars = EventPO.getNewParameterMap();
      vars.put("handle", resetRequest.getHandle());
      EventPO eventPO = EventPO.create(EventType.RESET_REQUESTED, vars);
      eventPO.apply(request);
      eventPO.setEventOwner(resetRequest.getHandle());
      eventRepository.save(eventPO);
    }
    return ResponseEntity.status(200).header("message", "If your reset request was for a valid user, a link was sent.").build();
  }

}
