package com.knunk.plus.services;

import com.knunk.plus.app.types.EventType;
import com.knunk.plus.app.types.Role;
import com.knunk.plus.repository.EventRepository;
import com.knunk.plus.repository.MemberRepository;
import com.knunk.plus.repository.po.EventPO;
import com.knunk.plus.repository.po.MemberPO;
import com.knunk.plus.rest.objects.PageResponse;
import com.knunk.plus.rest.objects.RegistrationRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

@Service
public class PlusRegistrationService {

  @Autowired
  private MemberRepository memberRepository;

  @Autowired
  private EventRepository eventRepository;

  @Autowired
  private PasswordEncoder passwordEncoder;

  public ResponseEntity<PageResponse> doRegistration(@RequestBody @Valid RegistrationRequest registrationRequest) {
    MemberPO memberPO = memberRepository.findMemberPOByHandle(registrationRequest.getHandle());
    if (memberPO != null) {
      return ResponseEntity.status(409).header("message", "User Already Exists").body(new PageResponse("hey, you already tried that!?"));
    }
    // filter gmail dots in email name.
    RegistrationRequest filteredRequest = applyRegistrationRequestFilters(registrationRequest);

    List<MemberPO> list = memberRepository.findAllByEmail(filteredRequest.getEmail());
    if (list.size() > 0) {
      return ResponseEntity.status(409).header("message", "Email Address Already Present").body(new PageResponse("It appears that email address is already registered. Try a password reset!"));
    }
    // add new member.
    memberPO = new MemberPO();
    memberPO.setLastLogin(LocalDateTime.now());
    memberPO.setInception(memberPO.getLastLogin());
    memberPO.setRole(Role.User);
    memberPO.setEmail(filteredRequest.getEmail());
    memberPO.setHandle(filteredRequest.getHandle());
    memberPO.setName(filteredRequest.getName());
    memberPO.setCodeword(passwordEncoder.encode(filteredRequest.getCodeword()));
    memberRepository.save(memberPO);
    // this somehow breaks authentication.
//    memberPO.setCodeword("?");

    Map<String, String> vars = EventPO.getNewParameterMap();
    vars.put("handle", filteredRequest.getHandle());
    EventPO eventPO = EventPO.create(EventType.NEW_USER_REGISTERED, vars);
    eventPO.setEventOwner(filteredRequest.getHandle());
    eventRepository.save(eventPO);

    return ResponseEntity.ok(new PageResponse("so, you're new here, " + memberPO.getName() + "?"));
  }

  public static RegistrationRequest applyRegistrationRequestFilters(RegistrationRequest request) {

    // create uniformity
    String email = request.getEmail().trim().toLowerCase();

    // handle gmail 'dots' problem.
    if (email.endsWith("gmail.com")) {
      String start = email.substring(0, email.indexOf("gmail.com"));
      start = start.replace(".", "");
      request.setEmail(start + "gmail.com");
    } else {
      request.setEmail(email);
    }

    // other logic here:

    return request;
  }
}
