package com.knunk.plus.app.types;

public enum Privacy {
  Public, FriendOfFriends, Friends, Private;
}
