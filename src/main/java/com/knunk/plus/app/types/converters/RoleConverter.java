package com.knunk.plus.app.types.converters;

import com.knunk.plus.app.types.Role;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class RoleConverter implements AttributeConverter<Role, String> {
  public String convertToDatabaseColumn(Role role) {
    return role.name();
  }

  public Role convertToEntityAttribute(String text) {
    return Role.valueOf(text);
  }
}
