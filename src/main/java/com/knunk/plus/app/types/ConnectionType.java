package com.knunk.plus.app.types;

public enum ConnectionType {
  None,
  Friends, FriendOfFriend,
  Partners,
  BlockedBy, Blocking, Enemy;

  public boolean areConnected() {
    return this.equals(Friends) || this.equals(FriendOfFriend) || this.equals(Partners);
  }
}
