package com.knunk.plus.app.types;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
  User,Admin,Owner;

  @Override
  public String getAuthority() {
    return name();
  }
}
