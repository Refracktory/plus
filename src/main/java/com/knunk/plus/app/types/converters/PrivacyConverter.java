package com.knunk.plus.app.types.converters;

import com.knunk.plus.app.types.Privacy;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class PrivacyConverter implements AttributeConverter<Privacy, String> {
    public String convertToDatabaseColumn(Privacy privacy) {
      return privacy.name();
    }

    public Privacy convertToEntityAttribute(String text) {
      return Privacy.valueOf(text);
    }
}
