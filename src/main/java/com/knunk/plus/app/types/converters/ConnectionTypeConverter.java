package com.knunk.plus.app.types.converters;

import com.knunk.plus.app.types.ConnectionType;

import javax.persistence.AttributeConverter;

public class ConnectionTypeConverter implements AttributeConverter<ConnectionType, String> {
  public String convertToDatabaseColumn(ConnectionType connectionType) {
    return connectionType.name();
  }

  public ConnectionType convertToEntityAttribute(String text) {
    return ConnectionType.valueOf(text);
  }
}

