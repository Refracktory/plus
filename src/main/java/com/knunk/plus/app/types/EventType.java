package com.knunk.plus.app.types;

public enum EventType {
  NEW_USER_REGISTERED("user {handle} registered"),
  USER_LOGIN("user {handle} log in"),
  USER_LOGOUT("user {handle} log out"),
  USER_REMOVED_SELF("user {handle} self-deleted"),
  USER_DELETED("user {handle} was deleted by {admin}"),
  USER_LOCKED("user {handle} was locked by {admin}"),
  USER_UNLOCKED("user {handle} was unlocked by {admin}"),
  USER_DISABLED("user {handle} was disabled by {admin}"),
  USER_ENABLED("user {handle} was enabled by {admin}"),
  CONNECTION_REQUEST("user {fromHandle} sent a connection request to {toHandle}"),
  MESSAGE_SENT("user {sender} sent a message to {receiver}"),
  RESET_REQUESTED("user {handle} requested a login reset");

  public String getTemplate() {
    return template;
  }

  private final String template;

  EventType(String template) {
    this.template = template;
  }
}
