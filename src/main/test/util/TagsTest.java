package util;

//
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.Test;
//
//class TagsTest {
//
//    @Test
//    void checkTags() {
//        Tags apples = new Tags();
//        apples.add("red").add("round").add("fruit");
//        Assertions.assertTrue(apples.contains("red"),"apples are red");
//        Assertions.assertTrue(apples.contains("round"),"apples are round");
//        Assertions.assertTrue(apples.contains("fruit"),"apples are fruit");
//        Assertions.assertFalse(apples.are("flaky"), "apples are flaky");
//    }
//
//    @Test
//    void checkNegatedTags() {
//        Tags apples = new Tags();
//        apples.add("red").add("round").add("fruit").not("vegetable");
//        Assertions.assertTrue(apples.are("red"),"apples are red");
//        Assertions.assertTrue(apples.are("round"),"apples are round");
//        Assertions.assertTrue(apples.are("fruit"),"apples are fruit");
//        Assertions.assertFalse(apples.are("flaky"), "apples aren't flaky");
//    }
//
//}