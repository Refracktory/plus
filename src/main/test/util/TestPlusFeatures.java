package util;

import com.knunk.tools.resttool.RestTool;

import unirest.HttpResponse;
import unirest.JsonNode;
import unirest.UnirestException;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class TestPlusFeatures {

  public static void main(String[] args) throws UnirestException, IOException {
    RestTool.addTitle("test default user cannot view events");
    RestTool.test(requestByDefaultUser("/admin/view/events"), 403);

    LocalDateTime before = LocalDateTime.now();
    String beforeTimeString = before.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);

    Map<String, String> parms = new HashMap<>();

    parms.put("startTime", beforeTimeString);
    parms.put("eventType", "RESET_REQUESTED");
    RestTool.addTitle("there are no current RESET_REQUESTED to view");
    RestTool.test(requestByDefaultAdminWithParameters("/admin/view/events", parms), 204);

    RestTool.addTitle("generate a RESET_REQUESTED event");
    parms = new HashMap<>();
    parms.put("handle", "refracktory");
    RestTool.test(requestByDefaultUserWithParameters("/log/reset", parms), 200);

    RestTool.addTitle("check for a current RESET_REQUESTED event");
    parms = new HashMap<>();
    parms.put("startTime", beforeTimeString);
    parms.put("eventType", "RESET_REQUESTED");
    RestTool.test(requestByDefaultAdminWithParameters("/admin/view/events", parms), 200);

    RestTool.addTitle("send a message to someone");
    parms = new HashMap<>();
    parms.put("handle", "refracktory");
    parms.put("message", "are you there, refracktory");
    RestTool.test(requestByDefaultUserWithParameters("/plus/send", parms), 200);

    System.out.println(RestTool.results());
  }

  private static HttpResponse<JsonNode> requestByDefaultUser(String uri) throws IOException{
    String handle = "refracktory";
    String pass = "refracktory";
    Map<String, String> parms = new HashMap<>();
    String json = RestTool.buildJsonBody(parms);
    RestTool.addCurrentTestName();
    return RestTool.postJsonFromRestServiceBasicAuth("local", "localhost_8080", uri, handle, pass, json);
  }

  private static HttpResponse<JsonNode> requestByDefaultUserWithParameters(String uri, Map<String, String> parms) throws IOException{
    String handle = "refracktory";
    String pass = "refracktory";
    String json = RestTool.buildJsonBody(parms);
    RestTool.addCurrentTestName();
    return RestTool.postJsonFromRestServiceBasicAuth("local", "localhost_8080", uri, handle, pass, json);
  }

  private static HttpResponse<JsonNode> requestByDefaultAdmin(String uri) throws IOException{
    String handle = "admin";
    String pass = "admin";
    Map<String, String> parms = new HashMap<>();
    String json = RestTool.buildJsonBody(parms);
    RestTool.addCurrentTestName();
    return RestTool.postJsonFromRestServiceBasicAuth("local", "localhost_8080", uri, handle, pass, json);
  }

  private static HttpResponse<JsonNode> requestByDefaultAdminWithParameters(String uri, Map<String, String> parms) throws IOException{
    String handle = "admin";
    String pass = "admin";
    String json = RestTool.buildJsonBody(parms);
    RestTool.addCurrentTestName();
    return RestTool.postJsonFromRestServiceBasicAuth("local", "localhost_8080", uri, handle, pass, json);
  }

  private static HttpResponse<JsonNode> view_events(String userToView) throws IOException {
    String url = "/admin/view/events";
    Map<String, String> parms = new HashMap<>();
    String json = RestTool.buildJsonBody(parms);
    RestTool.addCurrentTestName();
    return RestTool.postJsonFromRestServiceBasicAuth("local", "localhost_8080", url, "admin", "admin", json);
  }

}