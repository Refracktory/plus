package util;

import com.knunk.tools.resttool.RestTool;

import unirest.HttpResponse;
import unirest.JsonNode;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class TestPlusBase {

  public static HttpResponse<JsonNode> requestByDefaultUser(String uri) throws IOException {
    String handle = "refracktory";
    String pass = "refracktory";
    Map<String, String> parms = new HashMap<>();
    String json = RestTool.buildJsonBody(parms);
    RestTool.addCurrentTestName();
    return RestTool.postJsonFromRestServiceBasicAuth("local", "localhost_8080", uri, handle, pass, json);
  }

  public static HttpResponse<JsonNode> requestByDefaultUserWithParameters(String uri, Map<String, String> parms) throws IOException{
    String handle = "refracktory";
    String pass = "refracktory";
    String json = RestTool.buildJsonBody(parms);
    RestTool.addCurrentTestName();
    return RestTool.postJsonFromRestServiceBasicAuth("local", "localhost_8080", uri, handle, pass, json);
  }

  public static HttpResponse<JsonNode> requestByDefaultAdmin(String uri) throws IOException{
    String handle = "admin";
    String pass = "admin";
    Map<String, String> parms = new HashMap<>();
    String json = RestTool.buildJsonBody(parms);
    RestTool.addCurrentTestName();
    return RestTool.postJsonFromRestServiceBasicAuth("local", "localhost_8080", uri, handle, pass, json);
  }

  public static HttpResponse<JsonNode> requestByDefaultAdminWithParameters(String uri, Map<String, String> parms) throws IOException{
    String handle = "admin";
    String pass = "admin";
    String json = RestTool.buildJsonBody(parms);
    RestTool.addCurrentTestName();
    return RestTool.postJsonFromRestServiceBasicAuth("local", "localhost_8080", uri, handle, pass, json);
  }

  public static HttpResponse<JsonNode> view_events(String userToView) throws IOException {
    String url = "/admin/view/events";
    Map<String, String> parms = new HashMap<>();
    String json = RestTool.buildJsonBody(parms);
    RestTool.addCurrentTestName();
    return RestTool.postJsonFromRestServiceBasicAuth("local", "localhost_8080", url, "admin", "admin", json);
  }

}
