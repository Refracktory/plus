package util;

import com.knunk.tools.resttool.RestTool;

import unirest.HttpResponse;
import unirest.JsonNode;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class ShowEvents {

  public static void main(String[] args) throws IOException {
    LocalDateTime before = LocalDateTime.now();
    String beforeTimeString = before.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);

    Map<String, String> parms = new HashMap<>();
    parms.put("startTime", before.minusHours(1).format(DateTimeFormatter.ISO_DATE_TIME));
    parms.put("currentPage", "0");
    parms.put("pageSize", "20");

    RestTool.addTitle("show latest events");
    RestTool.test(requestByDefaultAdminWithParameters("/admin/view/events", parms), 200);

    RestTool.addTitle("show users");
    RestTool.test(requestByDefaultAdminWithParameters("/admin/view/users", parms), 200);

    System.out.println(RestTool.results());
  }

  private static HttpResponse<JsonNode> requestByDefaultAdminWithParameters(String uri, Map<String, String> parms) throws IOException {
    String handle = "admin";
    String pass = "admin";
    String json = RestTool.buildJsonBody(parms);
    RestTool.addCurrentTestName();
    return RestTool.postJsonFromRestServiceBasicAuth("local", "localhost_8080", uri, handle, pass, json);
  }

}
