package util;

import com.knunk.tools.resttool.RestTool;

import unirest.HttpResponse;
import unirest.JsonNode;
import unirest.UnirestException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class TestPlusConnections extends TestPlusBase {

  public static void main(String[] args) throws UnirestException, IOException {
    /**
     *     addTestUser("groupie1", Privacy.Public);
     *     addTestUser("groupie2", Privacy.Public);
     *     addTestUser("groupieP", Privacy.Private);
     *     addTestUser("groupieF", Privacy.Friends);
     *     addTestUser("groupieFoF", Privacy.FriendOfFriends);
     */
    RestTool.addTitle("make connection request from default user to test user");
    RestTool.test(connectionRequest("groupie1"), 200);
    System.out.println(RestTool.results());
  }

  public static HttpResponse<JsonNode> connectionRequest(String handleToConnectTo) throws IOException {
    String handle = "refracktory";
    String pass = "refracktory";
    String uri = "/connection/request";
    Map<String, String> parms = new HashMap<>();
    parms.put("toHandle", handleToConnectTo);
    parms.put("fromHandle", handle);
    String json = RestTool.buildJsonBody(parms);
    RestTool.addCurrentTestName();
    return RestTool.postJsonFromRestServiceBasicAuth("local", "localhost_8080", uri, handle, pass, json);
  }
}
