package util;

import com.knunk.tools.resttool.ResponseWrapper;
import com.knunk.tools.resttool.RestTool;

import org.json.JSONArray;
import unirest.HttpResponse;
import unirest.JsonNode;
import unirest.UnirestException;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class TestPlusMemberAdmin {

  public static void main_new_user(String[] args) throws UnirestException, IOException{
    RestTool.test(new_membership("test-only-user", "test-only-user"), 200);
    RestTool.test(view_membership("test-only-user", "owner", "owner"), 200);
    RestTool.test(view_users("owner", "owner"), 200);
    RestTool.test(accessPlus("/plus/hello", "test-only-user", "test-only-user"), 200);
  }

  public static void main(String[]args) throws UnirestException, IOException {

    RestTool.addTitle("test default user log in");
    RestTool.test(log_in("refracktory", "refracktory"), 200);
    RestTool.addTitle("test default user reset log in");
    RestTool.test(reset_login("refracktory", "refracktory"), 200);

    RestTool.addTitle("test 2nd registration of default user");
    RestTool.test(new_membership("refracktory", "refracktory"), 409);

    RestTool.addTitle("test registration with illegal username / password characters");
    RestTool.test(new_membership(":refracktory1", "refracktory"), 400);
    RestTool.test(new_membership("'refracktory2", "refracktory"), 400);
    RestTool.test(new_membership("\"refracktory3", "refracktory"), 400);
    RestTool.test(new_membership("refracktory1", ":refracktory"), 400);
    RestTool.test(new_membership("refracktory2", "'refracktory"), 400);
    RestTool.test(new_membership("refracktory3", "\"refracktory"), 400);

    RestTool.addTitle("test registration with illegal email addresses");
    RestTool.test(register_new_test_email("refracktory1", "refracktory", "jay.bass$gmail.com"), 400);
    RestTool.test(register_new_test_email("refracktory2", "refracktory", "jay.bass@gmail."), 400);
    RestTool.test(register_new_test_email("refracktory3", "refracktory", "jay.bass@gmail."), 400);
    RestTool.test(register_new_test_email("refracktory4", "refracktory", "@gmail.com"), 400);
    RestTool.test(register_new_test_email("refracktory5", "refracktory", "jay.bass@gmail..com"), 400);
    RestTool.test(register_new_test_email("refracktory6", "refracktory", ".....@gmail.com"), 400);

    RestTool.addTitle("test registration with duplicate email addresses");
    RestTool.test(register_new_test_email("emailer", "emailer", "email@gmail.com"), 200);
    RestTool.test(register_new_test_email("spammer", "spammer", "email@gmail.com"), 409);

    RestTool.addTitle("test registration with duplicate gmail address");
    RestTool.test(register_new_test_email("emailer1", "emailer1", "e.mail@gmail.com"), 409);

    RestTool.addTitle("delete test registration of duplicated gmail address");
    RestTool.test(delete_membership("emailer","owner", "owner"), 200);

    RestTool.addTitle("test non-available URLs with valid user");
    RestTool.test(accessPlus("/plus", "refracktory", "refracktory").getResponse(), 404);
    RestTool.test(accessPlus("/plus/NOT-REAL", "refracktory", "refracktory"), 404);

    RestTool.addTitle("test non-available URLs with invalid user");
    RestTool.test(accessPlus("/plus/NOT-REAL", "marco", "polo"), 401);

    RestTool.addTitle("test secured endpoint with unauthorized user");
    RestTool.test(accessPlus("/plus", "mildew", "mo;d"), 401);

    RestTool.addTitle("login");
    HttpResponse<JsonNode> loginResponse = log_in("refracktory", "refracktory");
    RestTool.testHeaderContains(loginResponse, "Plus-Token");
    RestTool.test(loginResponse, 200);

    RestTool.addTitle("regular access");
    RestTool.test(accessPlus("/plus", "refracktory", "refracktory"), 404);

    HttpResponse<JsonNode> response = accessPlus("/plus/hello", "refracktory", "refracktory")
      .testHeaderContains("Plus-Header").getResponse();
    RestTool.test(response, 200);

    RestTool.test(accessPlus("/plus/goodbye", "refracktory", "refracktory"), 200);

    RestTool.addTitle("test view membership for authority");
    RestTool.test(view_membership("refracktory", "owner", "owner"), 200);
    RestTool.test(view_membership("unknown-user", "owner", "owner"), 404);
    RestTool.test(view_membership("refracktory", "refracktory", "refracktory"), 403);
    RestTool.test(view_membership("unknown-user", "refracktory", "refracktory"), 403);

    RestTool.addTitle("test view events with authority");
    RestTool.test(view_events("refracktory", "owner", "owner"), 200);

    RestTool.addTitle("test view users with authority");
    RestTool.test(view_users("owner", "owner"), 200);

    HttpResponse<JsonNode> deleteIfNecessary = delete_membership("test-only-user","owner", "owner");
    RestTool.testReturnsAny(deleteIfNecessary, 200, 404, 409);

    RestTool.addTitle("add a test-only-user then check plus access");
    RestTool.test(new_membership("test-only-user", "test-only-user"), 200);
    HttpResponse<JsonNode> membership = view_membership("test-only-user", "owner", "owner");
    System.out.println(membership.getBody());
    RestTool.test(membership, 200);

    RestTool.addTitle("login test-only-user");
    loginResponse = log_in("test-only-user", "test-only-user");
    RestTool.testHeaderContains(loginResponse, "Plus-Token");
    RestTool.test(loginResponse, 200);

    RestTool.test(accessPlus("/plus/hello", "test-only-user", "test-only-user"), 200);
    RestTool.test(reset_login("test-only-user", "test-only-user"), 200);

    RestTool.addTitle("test destructive account methods for a user");
    RestTool.test(lock_membership("test-only-user", "test-only-user", "test-only-user"), 403);
    RestTool.test(delete_membership("test-only-user","test-only-user", "test-only-user"), 403);

    RestTool.addTitle("test owner deleting the test user then access a plus method from that user");
    RestTool.test(delete_membership("test-only-user","owner", "owner"), 200);
    RestTool.test(accessPlus("/plus/hello", "test-only-user", "test-only-user"), 401);

    System.out.println(RestTool.results());
  }

  private static ResponseWrapper<JsonNode> accessPlus(String uri, String handle, String pass) throws IOException{
    Map<String, String> parms = new HashMap<>();
    String json = RestTool.buildJsonBody(parms);
    RestTool.addCurrentTestName();
    return new ResponseWrapper(RestTool.postJsonFromRestServiceBasicAuth("local", "localhost_8080", uri, handle, pass, json));
  }

  private static HttpResponse<JsonNode> view_membership(String userToView, String handle, String pass) throws IOException {
    String url = "/admin/view/user";
    Map<String, String> parms = new HashMap<>();
    parms.put("handle", userToView);
    String json = RestTool.buildJsonBody(parms);
    RestTool.addCurrentTestName();
    return RestTool.postJsonFromRestServiceBasicAuth("local", "localhost_8080", url, handle, pass, json);
  }

  private static HttpResponse<JsonNode> view_events(String userToView, String handle, String pass) throws IOException {
    String url = "/admin/view/events";
    Map<String, String> parms = new HashMap<>();
    parms.put("startTime", LocalDateTime.now().minusHours(1).format(DateTimeFormatter.ISO_DATE_TIME));
    parms.put("currentPage", "1");
    parms.put("pageSize", "2");
    String json = RestTool.buildJsonBody(parms);
    RestTool.addCurrentTestName();
    return RestTool.postJsonFromRestServiceBasicAuth("local", "localhost_8080", url, handle, pass, json);
  }

  private static HttpResponse<JsonNode> view_users(String handle, String pass) throws IOException {
    String url = "/admin/view/users";
    Map<String, String> parms = new HashMap<>();
//    parms.put("startTime", LocalDateTime.now().minusHours(1).format(DateTimeFormatter.ISO_DATE_TIME));
    parms.put("currentPage", "1");
    parms.put("pageSize", "10");
    String json = RestTool.buildJsonBody(parms);
    RestTool.addCurrentTestName();
    HttpResponse<JsonNode> response = RestTool.postJsonFromRestServiceBasicAuth("local", "localhost_8080", url, handle, pass, json);
    JSONArray array = response.getBody().getArray();
    array.forEach(o -> System.out.println(o));
    return response;
  }

  private static HttpResponse<JsonNode> lock_membership(String userToLock, String handle, String pass) throws IOException {
    String url = "/admin/lock";
    Map<String, String> parms = new HashMap<>();
    parms.put("handle", userToLock);
    String json = RestTool.buildJsonBody(parms);
    RestTool.addCurrentTestName();
    return RestTool.postJsonFromRestServiceBasicAuth("local", "localhost_8080", url, handle, pass, json);
  }

  private static HttpResponse<JsonNode> delete_membership(String userToDelete, String handle, String pass) throws IOException {
    String url = "/admin/delete";
    Map<String, String> parms = new HashMap<>();
    parms.put("handle", userToDelete);
    String json = RestTool.buildJsonBody(parms);
    RestTool.addCurrentTestName();
    return RestTool.deleteJsonFromRestServiceBasicAuth("local", "localhost_8080", url, handle, pass, json);
  }

  private static HttpResponse<JsonNode> new_membership(String handle, String pass) throws IOException {
    String uri = "/register/new";
    Map<String, String> parms = new HashMap<>();

    parms.put("handle", handle);
    parms.put("codeword", pass);
    parms.put("password", pass);
    parms.put("name", "Jay Bass");
    parms.put("email", "jay.bass@gmail.com");

    String json = RestTool.buildJsonBody(parms);
    RestTool.addCurrentTestName();
    return RestTool.postJsonFromRestService("local", "localhost_8080", uri, "NONE", json);

  }

  private static HttpResponse<JsonNode> reset_login(String handle, String pass) throws IOException {
    String uri = "/log/reset";
    Map<String, String> parms = new HashMap<>();

    parms.put("handle", handle);
    parms.put("codeword", pass);

    String json = RestTool.buildJsonBody(parms);
    RestTool.addCurrentTestName();
    return RestTool.postJsonFromRestService("local", "localhost_8080", uri, "NONE", json);

  }

  private static HttpResponse<JsonNode> register_new_test_email(String handle, String pass, String email) throws IOException {
    String uri = "/register/new";
    Map<String, String> parms = new HashMap<>();

    parms.put("handle", handle);
    parms.put("codeword", pass);
    parms.put("name", "Jay Bass");
    parms.put("email", email);

    String json = RestTool.buildJsonBody(parms);
    RestTool.addCurrentTestName();
    return RestTool.postJsonFromRestService("local", "localhost_8080", uri, "NONE", json);

  }

  private static HttpResponse<JsonNode> log_in(String handle, String pass) throws IOException {
    String uri = "/log/in";
    Map<String, String> parms = new HashMap<>();
    parms.put("handle", handle);
    parms.put("code", pass);
    String json = RestTool.buildJsonBody(parms);
    return RestTool.postJsonFromRestService("local", "localhost_8080", uri, "NONE", json);

  }

}
